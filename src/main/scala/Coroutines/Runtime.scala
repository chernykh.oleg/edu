package Coroutines

import scala.concurrent.{ExecutionContext, Future}

object Runtime {
  import Computation._

  def run[A](c: Computation[A])(implicit mainContext: ExecutionContext,
                                rs: RuntimeScheduler): Future[A] =
    c match {
      case Return(a)       => Future.successful(a)
      case Suspend(resume) => rs.`yield`(resume)
      case FlatMap(sub, k: (Any => Computation[A])) =>
        sub match {
          case Return(a)       => run(k(a))
          case Suspend(resume) => rs.`yield`(resume).flatMap(res => run(k(res)))
          case FlatMap(sub, f: (Any => Computation[Any])) =>
            run(sub.flatMap(res => f(res).flatMap(k)))
        }
    }

  def apply[A](f: ComputationStep[A]): Computation[A] =
    Suspend(f)
}
