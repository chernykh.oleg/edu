package Coroutines

import java.util.concurrent.ExecutorService
import scala.concurrent.{Future, Promise}
import ComputationStep._
import scala.util.Try

class RuntimeScheduler(io: ExecutorService, cpu: ExecutorService) {
  def `yield`[A](cs: ComputationStep[A]): Future[A] = {
    val pool = definePool(cs)
    val promise = Promise[A]
    pool.submit(new Runnable {
      override def run(): Unit = {
        println(s"[thread pool hash]: ${pool.hashCode()}")
        Try {
          cs()
        }.fold(promise.failure, promise.success)
      }
    })
    promise.future
  }

  private def definePool(cs: ComputationStep[_]): ExecutorService =
    cs match {
      case CPUBounded(_) => cpu
      case IOBounded(_)  => io
    }
}
