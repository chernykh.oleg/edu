package Coroutines

sealed trait ComputationStep[A] extends (() => A) {
  override def apply(): A = f()
  protected val f: () => A
}

object ComputationStep {
  final case class CPUBounded[A](f: () => A) extends ComputationStep[A]
  final case class IOBounded[A](f: () => A) extends ComputationStep[A]

  implicit class ops[A](f: () => A) {
    def asIOBounded(): Computation[A] = Runtime {
      { IOBounded { f } }
    }

    def asCPUBounded(): Computation[A] = Runtime {
      { CPUBounded { f } }
    }
  }
}
