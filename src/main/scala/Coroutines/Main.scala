package Coroutines

import scala.concurrent.ExecutionContext.Implicits._
import scala.io.StdIn.readLine
import ComputationStep._

import java.util.concurrent.Executors
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object Main extends App {
  def mockHeavyCpuTask(sleepTime: Long, id: String) =
    (() => {
      println(s"Heavy cpu task $id")
      Thread.sleep(sleepTime)
    }).asCPUBounded()

  def mockIOTask(str: String, sleepTime: Long) =
    (() => {
      println("io task")
      Thread.sleep(sleepTime)
      println(s"io finished $str")
    }).asIOBounded()

  // pool for example
  val ioPool = Executors.newFixedThreadPool(2)
  val cpuPool = Executors.newFixedThreadPool(2)
  println(
    s"[io pool id]: ${ioPool.hashCode()}, [cpu pool id]: ${cpuPool.hashCode()}"
  )
  implicit val rs = new RuntimeScheduler(ioPool, cpuPool)

  val coroutine1 = for {
    _ <- mockHeavyCpuTask(2000, "task1.1")
    _ <- mockIOTask("task1.1", 3000)
    _ <- mockHeavyCpuTask(3000, "task1.2")
    _ <- mockHeavyCpuTask(4000, "task1.3")
  } yield ()

  val coroutine2 = for {
    _ <- mockHeavyCpuTask(5000, "task2.1")
    _ <- mockIOTask("task2.1", 3000)
    _ <- mockHeavyCpuTask(3000, "task2.2")
    _ <- mockHeavyCpuTask(2000, "task2.3")
    _ <- mockIOTask("task2.1", 4000)
  } yield ()

  // example of use
  val f1 = Runtime.run(coroutine1).andThen(_ => println("coroutine1 fulfilled"))
  val f2 = Runtime.run(coroutine2).andThen(_ => println("coroutine2 fulfilled"))
  Await.result(f1, Duration.Inf)
  Await.result(f2, Duration.Inf)
  ioPool.shutdown()
  cpuPool.shutdown()
}
