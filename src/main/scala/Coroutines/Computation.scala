package Coroutines

import Computation._

sealed trait Computation[A] {
  def flatMap[B](f: A => Computation[B]): Computation[B] = FlatMap(this, f)
  def map[B](f: A => B): Computation[B] = flatMap(f.andThen(Return(_)))
}

object Computation {
  case class Return[A](a: A) extends Computation[A]
  case class Suspend[A](resume: ComputationStep[A]) extends Computation[A]
  case class FlatMap[A, B](c: Computation[A], k: A => Computation[B])
      extends Computation[B]
}
