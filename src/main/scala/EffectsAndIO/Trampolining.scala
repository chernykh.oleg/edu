package EffectsAndIO

import scala.annotation.tailrec
import scala.io.StdIn.readLine

object Trampolining extends App {
  case class Dispatcher() {}

  sealed trait TailRec[A] {
    def flatMap[B](f: A => TailRec[B]): TailRec[B] = FlatMap(this, f)
    def map[B](f: A => B): TailRec[B] = flatMap(f.andThen(Return(_)))
  }

  case class Return[A](a: A) extends TailRec[A]
  case class Suspend[A](resume: () => A) extends TailRec[A]
  case class FlatMap[A, B](sub: TailRec[A], k: A => TailRec[B])
      extends TailRec[B]

  object IO {
    @tailrec def run[A](io: TailRec[A]): A = io match {
      case Return(a)       => a
      case Suspend(resume) => resume()
      case FlatMap(sub, k: (Any => TailRec[A])) =>
        sub match {
          case Return(a)       => run(k(a))
          case Suspend(resume) => run(k(resume()))
          case FlatMap(sub, f: (Any => TailRec[Any])) =>
            run(sub.flatMap(res => f(res).flatMap(k)))
        }
    }

    def apply[A](f: => A): TailRec[A] = Suspend(() => f)
  }

  def printL(s: String): TailRec[Unit] = IO { println(s) }

  def readL(): TailRec[String] = IO { readLine() }

  val io = for {
    _ <- printL("hello")
    _ <- printL("hello")
    _ <- printL(s"world")
    _ <- printL("!")
  } yield ()

  IO.run(io)
}
