package EffectsAndIO

import scala.io.StdIn.readLine

/**
  * Points:
  * 1. There is important distinguish between side-effects and effects
  *
  * 2. IO monad provides a straightforward way of embedding imperative programming with I/O effects in a pure program
  * while preserving a referential transparency. It clearly separates effectful code
  * (code that needs to have some effect on the outside world) - from the rest of out program
  *
  * 3. There is a technique for dealing with effects - using pure functions to compute
  * a DESCRIPTION OF EFFECTFUL COMPUTATION, which is than executed by a separate INTERPRETER
  * that actually performs those effects.
  *
  * 4. Insight: inside every function with side effects (impure) sits a pure function waiting to get out.
  * Formalize: Given an impure function f: A => B. We can split f into two functions:
  * 1) A pure function A => D where D is some DESCRIPTION of the result f
  * 2) An impure function of type D => B which can be thought of us as INTERPRETER of these DESCRIPTION
  *
  * 5. IO monad CLEARLY SEPARATES PURE CODE FROM IMPURE CODE
  */
object EffectsAndIO extends App {
  case class Player(name: String, score: Int)

  def winner(p1: Player, p2: Player): Option[Player] =
    if (p1.score > p2.score) Some(p1)
    else if (p1.score < p2.score) Some(p2)
    else None

  def winnerMsg(p: Option[Player]): String =
    p map {
      case Player(name, _) => s"$name is the winner!"
    } getOrElse "It's a draw."

  val p1 = Player("Nick", 20)
  val p2 = Player("Sam", 30)

  {

    /**
      * Note how the side effect, println, is now only in the outermost layer of the program,
      * and what’s inside the call to println is a pure expression.
      */
    def contest(p1: Player, p2: Player): Unit =
      println(winnerMsg(winner(p1, p2)))

    contest(p1, p2)
  }

  {
    trait IO { def run(): Unit }

    def PrintLine(str: String): IO = new IO {
      override def run(): Unit = println(str)
    }

    /**
      * Now contest is pure! It returns an IO value, which DESCRIBES an action that need to take place.
      *
      * WE SAY THAT CONTEST HAS AN EFFECT (OF EFFECTFUL), BUT IT'S ONLY INTERPRETER OF IO (IT'S RUN METHOD) THAT
      * ACTUALLY HAS SIDE EFFECT
      */
    def contest(p1: Player, p2: Player): IO =
      PrintLine(winnerMsg(winner(p1, p2)))

    contest(p1, p2).run()
  }

  {
    def fahrenheitToCelsius(f: Double): Double =
      (f - 32) * 5.0 / 9.0

//    def farToCel: Unit = {
//      println("Enter a temperature in degrees Fahrenheit: ")
//      val d = readLine.toDouble
//      println(fahrenheitToCelsius(d))
//    }
//
//    farToCel

    /**
      * we want to convert convert into pure function
      */
    /**
      * what is the problem?
      * 1. Not stack safe (trampolining)
      * 2. IO doesn't describe WHAT EXACTLY effect under the hood
      * 3. How to make IO async and non-blocking?
      */
    sealed trait IO[A] { self =>
      def run(): A
      def flatMap[B](f: A => IO[B]): IO[B] = new IO[B] {
        override def run(): B = f(self.run()).run()
      }
      def map[B](f: A => B): IO[B] = new IO[B] {
        override def run(): B = f(self.run())
      }
    }

    object IO {
      def apply[A](f: => A): IO[A] = new IO[A] {
        override def run(): A = f
      }
    }

    def ReadLine(): IO[String] = IO { readLine() }
    def PrintLine(line: String): IO[Unit] = IO { println(line) }

    /**
      * Now function is pure - has no side effects, but describes effectful computation
      * (REFERENTIAL TRANSPARENT FUNCTION WHICH DESCRIBES A COMPUTATION WITH SIDE EFFECTS)`
      *
      * Method run is the interpreter which execute description and it has a side effects
      */
    def converter: IO[Unit] =
      for {
        _ <- PrintLine("input number: ")
        number <- ReadLine().map(_.toDouble)
        _ <- PrintLine(fahrenheitToCelsius(number).toString)
      } yield ()

    converter.run()
  }
}
